<?php

namespace Drupal\entity_collector;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * PrerenderItem callback.
 */
class PrerenderItem implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['callback'];
  }

  /**
   * Add javascript support to collection items.
   *
   * @param array $variables
   *   Render array.
   *
   * @return array
   *   Altered render array.
   */
  public static function callback($variables) {
    $variables['#cache']['contexts'][] = 'route.name';

    $variables['#attributes']['class'][] = 'js-entity-collection-item';

    /** @var \Drupal\entity_collector\Entity\EntityCollectionInterface $entity */
    $entity = NULL;

    if (isset($variables['#object'])) {
      $entity = $variables['#object'];
    }

    if ($entity === NULL && isset($variables['#entity'])) {
      $entity = $variables['#entity'];
    }

    $key = '#' . $variables['#source'];
    if ($entity === NULL && isset($variables[$key])) {
      $entity = $variables['#' . $variables['#source']];
    }

    if ($entity === NULL) {
      return $variables;
    }

    $variables['#attributes']['data-entity-id'] = $entity->id();

    return $variables;
  }

}
