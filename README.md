CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Collection Bar
 * Maintainers


INTRODUCTION
------------

Collections are sets of entities that are gathered for a specific purpose.
You can create entity collection types at ```admin/structure/collection-types```
and manage the settings. There will automatically be a field generated for the
collection type based on the entity where you want to create the collection for.

For example, you could create a Entity Collection Type `Media` and add all sorts
of media entities to it and create a collection. For each entity collection type
there will be two fields exposed, an `add` and a `remove` field. This is done by
the [Extra Field](https://www.drupal.org/project/extra_field) module. These
fields make it possible to add/remove the entities to/from the collection
through the interface to your active collection bar (see `Collection Bar`
below).

You can for example create a view mode containing a thumbnail and the add and
remove field, so you can create a view with a list of entities with that view
mode and manage your collection.

A collection can be shared with different users by adding them as a participant
to the entity collection.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/entity_collector

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/entity_collector


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Entity Collector module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


COLLECTION BAR
--------------

Navigate to Administration > Structure > Entity collection type entities to add
entity collection types.

On each page in the collection bar region the active collection is shown. In the
block settings at Administration > Structure > Block layout and place an Entity
Collection Block you will be able to select:
 - Entity collection type
    - Which entity collection type should be shown.
 - Entity Collection view Mode
    - Which view mode should be used to render the entities.

Besides collecting entities it is also possible to switch between active
collections or create new ones. The collection bar on the bottom of
many pages has the option to trigger this through a modal popup.

Selecting an existing collection in the modal will go through an AJAX
request and will replace the collection bar on the bottom with the
selected collection. While the creation of a new collection will be
a normal page request form submission and will reload the page with
the newly created collection as the active one.


MAINTAINERS
-----------

 * Jeffrey Bertoen (jefuri) - https://www.drupal.org/u/jefuri
